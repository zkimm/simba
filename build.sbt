name := "Simba"

version := "1.0"

val sparkVersion = "1.3.1"

scalaVersion := "2.10.4"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
resolvers += "Local Maven Repository" at "/root/.m2/repository"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-library" % "2.10.4",
  "org.scala-lang" % "scala-compiler" % "2.10.4",
  "org.scala-lang" % "scala-reflect" % "2.10.4"
)

libraryDependencies ++= Seq(
  "com.thinkaurelius.titan" % "titan-core" % "1.0.0",
  "com.thinkaurelius.titan" % "titan-es" % "1.0.0",
  "com.thinkaurelius.titan" % "titan-berkeleyje" % "1.0.0",
  "com.thinkaurelius.titan" % "titan-hbase" % "1.0.0",
  "org.apache.tinkerpop" % "gremlin-driver" % "3.0.1-incubating", 
  "org.apache.tinkerpop" % "gremlin-core" % "3.0.1-incubating",
  "org.apache.spark" %% "spark-core" % sparkVersion
)

//test
//libraryDependencies ++= Seq(
//  "com.thinkaurelius.titan" % "titan-core" % "1.0.0" % "test", 
//  "com.thinkaurelius.titan" % "titan-es" % "1.0.0" % "test",
//  "com.thinkaurelius.titan" % "titan-berkeleyje" % "1.0.0" % "test",
//  "com.thinkaurelius.titan" % "titan-hbase" % "1.0.0" % "test",
//  "org.apache.tinkerpop" % "gremlin-driver" % "3.0.1-incubating" % "test",
//  "org.apache.tinkerpop" % "gremlin-core" % "3.0.1-incubating" % "test",
//  "org.apache.spark" %% "spark-core" % sparkVersion % "test"
//)

libraryDependencies += "org.scalactic" %% "scalactic" % "2.2.6"
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6" % "test"

mainClass in (Compile, run) := Some("simba.GraphOfTheGodsExample")

scalacOptions += "-deprecation"
mainClass in assembly := Some("simba.GraphOfTheGodsExample")
test in assembly := {}

assemblyMergeStrategy in assembly := {
case x if Assembly.isConfigFile(x) =>
  MergeStrategy.concat
case PathList(ps @ _*) if Assembly.isReadme(ps.last) || Assembly.isLicenseFile(ps.last) =>
  MergeStrategy.rename
case PathList("META-INF", xs @ _*) =>
  (xs map {_.toLowerCase}) match {
    case ("manifest.mf" :: Nil) | ("index.list" :: Nil) | ("dependencies" :: Nil) =>
      MergeStrategy.discard
    case ps @ (x :: xs) if ps.last.endsWith(".sf") || ps.last.endsWith(".dsa") =>
      MergeStrategy.discard
    case "plexus" :: xs =>
      MergeStrategy.discard
    case "services" :: xs =>
      MergeStrategy.filterDistinctLines
    case ("spring.schemas" :: Nil) | ("spring.handlers" :: Nil) =>
      MergeStrategy.filterDistinctLines
    case _ => MergeStrategy.first
  }
case _ => MergeStrategy.first
}

