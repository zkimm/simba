package simba

import com.thinkaurelius.titan.core.{TitanGraph, TitanFactory}
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest._
import simba.titan100.TitanSimbaDB

/**
  * Created by liliang on 2017/2/28.
  * compares the rdd of docs and links after operaations insert, insertLinks, etc.
  */

class TitanSimbaDBTest extends WordSpec with Matchers with BeforeAndAfterAll {
  //
  val conf = new SparkConf().setMaster("local[2]").setAppName("Test")
  implicit val sc = new SparkContext(conf)

  //clear simba graph, assume the storage backend is hbase and index backend is es, there are configured in conf/titan-hbase-es-simba.properties
  val titanConf = "conf/titan-hbase-es-simba.properties"
  var graph:TitanGraph = TitanFactory.open(titanConf)
  graph.close()
  com.thinkaurelius.titan.core.util.TitanCleanup.clear(graph)

  val gdb = TitanSimbaDB(sc, titanConf)

  val docRDD = sc.objectFile[Document]("test_input/godsDocs")
  val linkRDD = sc.objectFile[DocumentLink]("test_input/godsLinks")

  gdb.insert(docRDD)
  gdb.insertLinks(linkRDD)

  docRDD.foreach(s => s.simbaPrint())
  println("linkRDD count " + linkRDD.count())
  linkRDD.foreach(s => s.print())

  //
  "simbaDocRDD" should{
    "have the same count" in {
      val expected = docRDD.count()
      gdb.docs().map(s=>s.get("name").toString).count() shouldBe expected
    }
    "have the same elements" in {
      val expected = docRDD.map(s => s.get("name") match {
        case Some(s) => s
        case None => "?"
      }).sortBy(s => s.toString).collect()
      gdb.docs().map(s => s).map(s => s.get("name") match {
        case Some(s) => s
        case None => "?"
      }).sortBy(s => s.toString).collect() shouldBe expected
    }
  }

  linkRDD.map(s => s.label).foreach(println)
  gdb.links().map(s => s.label).foreach(println)

  "simbaLinkRDD" should {
    "have the same count" in {
      val expected = linkRDD.count()
      gdb.links().count() shouldBe expected
    }
    "have the same label" in {
      val expected = linkRDD.map(s => s.label).collect()
      gdb.links().map(s => s.label).collect()
    }
  }

  override def afterAll(): Unit = gdb.close()
}
