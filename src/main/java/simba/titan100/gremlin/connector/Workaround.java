package simba.titan100.gremlin.connector;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;

/**
 * Created by cnic-liliang on 2016/12/20.
 */

public class Workaround {
    public static <E> GraphTraversal<?, E> cloneTraversal(GraphTraversal<?, E> traversal) {
        return traversal.asAdmin().clone();
    }
}
