package shell

import org.apache.spark.{SparkContext, SparkConf}

import scala.tools.nsc.Settings
import scala.tools.nsc.interpreter.ILoop

object simbaRepl {

	def foo()= new Object{
		override def toString()="!@#$%^";
	}
	
	def main(args: Array[String]): Unit = {

//    val conf = new SparkConf().setMaster("spark://10.0.80.42:7077").setAppName("Test")
//    println("====================")
//    implicit val sc = new SparkContext(conf)
//    println("------------------------------")

    val repl = new ILoop {
      override def createInterpreter() = {
        super.createInterpreter()
//        intp.bind("Document",    "Sting",  " interface of bigdatalab of cnic")
//        intp.bind("SimbaDocument", "String", " Document Type")
      }

      def simba_logo():String = {
        val simba_string = " _____ _           _           \n  / ____(_)         | |          \n | (___  _ _ __ ___ | |__   __ _ \n  \\___ \\| | '_ ` _ \\| '_ \\ / _` |\n  ____) | | | | | | | |_) | (_| |\n |_____/|_|_| |_| |_|_.__/ \\__,_|"
        simba_string
      }

      override def loadFiles(settings: Settings): Unit = {
        intp.beQuietDuring{
          command(""" System.setProperty("SPARK_SUBMIT","true")""")
          command("""System.setProperty("CLASSPATH", "/usr/local/spark/spark-1.6.1-bin-hadoop2.6/lib/spark-examples-1.6.1-hadoop2.6.0.jar:/opt/spark-1.6.1-bin-hadoop2.6/lib/spark-assembly-1.6.1-hadoop2.6.0.jar:/opt/spark-1.6.1-bin-hadoop2.6/lib/spark-1.6.1-yarn-shuffle.jar:/opt/spark-1.6.1-bin-hadoop2.6/lib/datanucleus-rdbms-3.2.9.jar:/opt/spark-1.6.1-bin-hadoop2.6/lib/datanucleus-core-3.2.10.jar:/opt/spark-1.6.1-bin-hadoop2.6/lib/datanucleus-api-jdo-3.2.6.jar:")""")
          command("""System.setProperty("spark.submit.deployMode","client")""")
          command("""System.setProperty("spark.master","local[2]")""")
          //command("""System.setProperty("spark.jars","./target/scala-2.10/Simba-assembly-1.0.jar")""")
          command("""System.setProperty("spark.jars","./target/scala-2.10/simba-2.10-1.0.jar")""")
          command("""import org.apache.spark.{SparkContext, SparkConf}""")
          command("""val conf = new SparkConf().setMaster("local[2]").setAppName("Test")""")
          command("""val sc = new SparkContext(conf)""")

          command("import simba._")
          command("import simba.titan100._")
          command("import simba.titan100.TitanSimbaDB")
          command("import simba.titan100.connector._")
          command("""val sim = new SimbaDB""")
          replayCommandStack = Nil
        }
        super.loadFiles(settings)
      }

      override def printWelcome(): Unit = {
        val str = simba_logo()
        Console println str
        Console println "invented by CNIC"
      }
      override def prompt = "simba>>>"
    }

    val settings = new Settings
    settings.Yreplsync.value = true
    settings.deprecation.value = true
    //use when launching normally outside SBT
    settings.usejavacp.value = true
    System.setProperty("scala.repl.prompt", "simba>")
    if(false)
      System.setProperty("scala.repl.welcome", """Welcome to simba! \nCopyRight www.cnic.cn""")

    repl.process(settings)

	}
}
