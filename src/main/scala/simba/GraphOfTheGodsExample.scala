package simba

import com.thinkaurelius.titan.core.{TitanFactory, TitanGraph}
import com.thinkaurelius.titan.example.GraphOfTheGodsFactory
import org.apache.spark.{SparkConf, SparkContext}
import simba.titan100.TitanGraphOfTheGodsExample

/**
  * Created by liliang on 2017/2/27
  */
object GraphOfTheGodsExample {
  //set spark cluster environment
//  System.setProperty("SPARK_SUBMIT","true")
//  System.setProperty("CLASSPATH", "/opt/spark-1.6.1-bin-hadoop2.6/lib/spark-examples-1.6.1-hadoop2.6.0.jar:/opt/spark-1.6.1-bin-hadoop2.6/lib/spark-assembly-1.6.1-hadoop2.6.0.jar:/opt/spark-1.6.1-bin-hadoop2.6/lib/spark-1.6.1-yarn-shuffle.jar:/opt/spark-1.6.1-bin-hadoop2.6/lib/datanucleus-rdbms-3.2.9.jar:/opt/spark-1.6.1-bin-hadoop2.6/lib/datanucleus-core-3.2.10.jar:/opt/spark-1.6.1-bin-hadoop2.6/lib/datanucleus-api-jdo-3.2.6.jar:")
//  System.setProperty("spark.submit.deployMode","client")
//  System.setProperty("spark.master","local[2]")
//  System.setProperty("spark.jars","./target/scala-2.11/simba_2.11-1.0.jar")

  def main(args: Array[String]): Unit = {
    //initialize Spark Context
    val conf = new SparkConf().setMaster("local[2]").setAppName("Test")
    implicit val sc = new SparkContext(conf)

    val titanConf = "conf/titan-hbase-es-simba.properties"
    val docObjectFile = "test_input/godsDocs"
    val refObjectFile = "test_input/godsLinks"

    var graph:TitanGraph = TitanFactory.open(titanConf)
    graph.close()
    com.thinkaurelius.titan.core.util.TitanCleanup.clear(graph)
    graph = TitanFactory.open(titanConf)
    GraphOfTheGodsFactory.load(graph)
    println(graph.traversal().V().count().next())
    graph.close()

    val titanDBTest = new TitanGraphOfTheGodsExample(titanConf, docObjectFile, refObjectFile)
    titanDBTest.run(sc)
  }
}
