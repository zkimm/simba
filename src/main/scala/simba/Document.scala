package simba

import java.util

import org.apache.commons.configuration.Configuration

import scala.collection.mutable.ArrayBuffer
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal

/*
  * liliang@cnic.cn
 */

//ObjectID
class ObjectID(id:String) extends Serializable {
  var _id = id

  override def toString():String = _id

  def get():String = _id
  def update(_nid:String): Boolean = {
    _id = _nid
    true
  }
}

object ObjectID {
  def  apply(id: String) = new ObjectID(id)
}

//Document
class Document extends Serializable {
  val doc = scala.collection.mutable.Map[String, Any]()
  var objectID:ObjectID = new ObjectID("M1n45dkz")

  def iterator() = doc.toIterator
  def objID() = objectID

  def setLabel(label: String) = { doc += ("label" -> label)}

  def add(key: String, value: Any): Boolean = {
    //scala 10.4  doc += (key -> value)
    doc(key) = value
    return true
  }

  def addObjectID(objID: ObjectID) = {
    objectID = objID
    //println(objectID.get())
  }

  def del(key:String): Boolean = {
    var res = true
    if(doc.contains(key)) {doc.remove(key)}
    else {res = false}
    res
  }

  def get(key:String): Any = {
    doc.get(key)
  }

  def simbaPrint(): Unit= {
    println("ObjectID: " + objectID.toString())
    val res = doc.toIterator
    while(res.hasNext){
      val v = res.next()
      if(v._2.getClass.toString.substring(6).compareTo("simba.DocumentEmb")==0) {
        val emDoc = v._2.asInstanceOf[Document]
        //println("---DocumentEmb begin----")
        emDoc.simbaPrint()
        //println("---DocumentEmb end---")
      }
      else {
        println(v.toString())
      }
    }
  }
}
object Document {
  def apply() = new Document()
}

//embeded document
class DocumentEmb extends Document{
  //
}
object DocumentEmb {
  def apply() = new DocumentEmb
}

//DocumentRef
class DocumentLink extends Serializable {
  var label:String = "link"
  var srcID:ObjectID = new ObjectID("")
  var objID:ObjectID = new ObjectID("")
  val propertyPairs = scala.collection.mutable.Map[String, Any]()

  def refSet(src:String, obj:String) = {
    srcID = new ObjectID(src)
    objID = new ObjectID(obj)
  }

  override def toString: String = srcID.toString() + "->" + "(" + label + ")->" + objID.toString

  def link(v: String) = {
    label = v
  }

  def add(key: String, value: Any): Boolean = {
    propertyPairs += (key -> value)
    return true
  }

  def print() = {
    println("label " + label)
    println("from " + from() + " to " + to())
    println("property_pairs: ")
    val res = propertyPairs.toIterator
    while(res.hasNext) {
      val v = res.next()
      println(v.toString())
    }
  }

  def from() = srcID
  def to()  = objID
}

object DocumentLink {
  def apply() = new DocumentLink()
}

