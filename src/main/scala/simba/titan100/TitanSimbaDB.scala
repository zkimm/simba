package simba.titan100

/**
 * Created by cnic-liliang on 2016/12/13.
 */

import com.thinkaurelius.titan.core.{TitanFactory, TitanGraph}
import com.thinkaurelius.titan.core.schema.{SchemaAction, SchemaStatus, TitanManagement}
import com.thinkaurelius.titan.graphdb.database.management.ManagementSystem
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.{GraphTraversalSource, __}
import org.apache.tinkerpop.gremlin.structure.{Edge, T, Vertex}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

import simba._
import simba.titan100.connector._

class TitanSimbaDB(_sc:SparkContext, titanConf: String) extends SimbaDB {
  implicit val sc = _sc
  var graph:TitanGraph = TitanFactory.open(titanConf)
  //build index for docID, type, etc.
  titanIndexCreate()

  def titanIndexCreate() = {
    var mgmt = graph.openManagement()
    val iter = mgmt.getOpenInstances.iterator()
    while(iter.hasNext) {
      val v = iter.next()
      if(v.contains("current")==false) mgmt.forceCloseInstance(v)
    }
    mgmt.commit()
    //create index here
    indexCreate("docID", "bydocIDXComposite")
  }

  def indexCreate(propertyName:String, indexName: String) = {
    graph.tx().commit()
    var mgmt = graph.openManagement()
    var property = mgmt.getPropertyKey(propertyName)
    if(property == null) property = mgmt.makePropertyKey(propertyName).dataType(classOf[String]).make()

    var index = mgmt.getGraphIndex(indexName)

    if(index != null && index.getIndexStatus(property).toString.compareTo("ENABLED")==0) {}
    else {
      if(index == null)
        mgmt.buildIndex(indexName, classOf[Vertex]).addKey(property).buildCompositeIndex()

      mgmt.commit()

      //awaitGraphIndexStatus
      ManagementSystem.awaitGraphIndexStatus(graph, indexName).status(SchemaStatus.REGISTERED).call().getSucceeded

      //update index
      mgmt = graph.openManagement()
      property = mgmt.getPropertyKey(propertyName)
      index = mgmt.getGraphIndex(indexName)
      if(index.getIndexStatus(property).toString.compareTo("ENABLED") != 0)
        mgmt.updateIndex(mgmt.getGraphIndex(indexName), SchemaAction.REINDEX).get()
      mgmt.commit()
    }
  }

  var g:GraphTraversalSource = graph.traversal()

  def close() = {
    graph.close()
  }

  //docs set of TitanSimbaDB, assume the size of Documents in TitanDB is larger than zero
  def docsToObjectFile(docFile: String) = {
    //give objectID to each doc
    val iter = g.V()
    var cnt = 0
    while(iter.hasNext) {
      val v = iter.next()
      val id = v.id()
      v.property("docID", id.toString)
      cnt += 1
      if(cnt % 10000 == 0) {
        println(cnt)
        //if(cnt % 1000000 == 0)
          println("-------- cnt " + cnt + "---------")
        graph.tx().commit()
      }
    }
    graph.tx().commit()
    //now save doc data to objectfile
    val tra = g.V()
    val real = tra.toDocRDD()
    real.map(s=>s).saveAsObjectFile(docFile)
  }

  //links set of TitanSimbaDB
  def linksToObjectFile(refFile: String) = {
    val tra = g.E()
    val real = tra.toRefRDD()
    real.map(s=>s).saveAsObjectFile(refFile)
  }

  //docs of TitanSimbaDB, assume the size of Documents in TitanDB is larger than zero
  override def docs(): RDD[Document] = {
    val traveral = g.V()
    val vertexRDD = traveral.toDocRDD().map(v => v)
    vertexRDD
  }

  //refs of TitanSimbaDB
  override  def links(): RDD[DocumentLink] = {
    val traversal = g.E()
    val edgeRDD = traversal.toRefRDD().map(e => e)
    edgeRDD
  }

  //property create
  def propertyCreateX(rdd: RDD[Document]) = {

    var mgmt = graph.openManagement()

    val drRDD = rdd
    val driter = drRDD.collect().toIterator

    graph.tx().rollback()
    mgmt = graph.openManagement()
    val mdriter = drRDD.collect().toIterator
    while(mdriter.hasNext) {
      val doc = mdriter.next()
      val vertexLabel = doc.get("label")
      var vertexManagelabel = mgmt.getVertexLabel(vertexLabel.toString)
      if(vertexManagelabel == null) {
        vertexManagelabel = mgmt.makeVertexLabel(vertexLabel.toString).make()
      }
      val iter = doc.iterator()
      while(iter.hasNext){
        val kv = iter.next()
        val key = kv._1
        val value = kv._2.toString
        //println(key + " " + value)
        if(key.compareTo("label")==0) {
          if (mgmt.containsVertexLabel(value) == false)
            mgmt.makeVertexLabel(value).make()
        }
        else {
          if(mgmt.containsPropertyKey(key) == false) {
            mgmt.makePropertyKey(key).dataType(classOf[String]).make()
          }
        }
      }
    }

    mgmt.commit()
  }

  //Insert docs rdd into TitanSimbaDB
  override def insert(rdd: RDD[Document]) = {
    //Titan Schema and Index Update
    graph.tx().rollback()
    propertyCreateX(rdd)

    var mgmt = graph.openManagement()

    val drRDD = rdd
    val driter = drRDD.collect().toIterator

    //insert new vertex and update exsited vertex
    var check = drRDD.map(s => s.objID().get().toString).collect().toList
    if(graph.traversal().V().hasNext) {
      val srcRDD = drRDD.map(s => s.objID().get().toString)
      val dstRDD = docs().map(s => s.objID().get().toString)
      check =srcRDD.subtract(dstRDD).collect().toList
    }

    graph.tx.rollback()

    while (driter.hasNext) {
      val doc = driter.next()
      val objID = doc.objID()

      var vertex = check.contains(doc.objID().get()) match {
        case false =>
          //println(graph.traversal().V().has("docID", doc.objectID.get()).hasNext)
          graph.traversal().V().has("docID", doc.objectID.get()).next()
        case true =>
          graph.addVertex(T.label, doc.get("label").toString)
      }
      vertex.property("docID", objID.get())
      //update properties
      var iter = doc.iterator()
      while (iter.hasNext) {
        val kv = iter.next()
        if (kv._1.compareTo("label") != 0) {
          //          val value = vertex.value[String](kv._1)
          //          println(value)
          if(mgmt.containsPropertyKey(kv._1))
            vertex.property(kv._1, kv._2.toString)
          else {
            println(" Property " + kv._1 + " not exists!!!")
            System.exit(-1)
          }
        }
      }
    }
    graph.tx.commit()

    true
  }

  //Insert DocumentRef into TitanSimbaDB
  override def insertLinks(rdd:RDD[DocumentLink]) = {
    val g = graph.traversal()
    //edge index

    graph.tx().rollback()
    //Titan insert
    val driter = rdd.collect().toIterator
    while(driter.hasNext) {
      val iter = driter.next
      //iter.print()
      //from to ID
      val srcID = iter.srcID
      var objID = iter.objID
      //from to Vertex
      var srcVertex:Vertex = null
      var objVertex:Vertex = null
      //check
      if(g.V().has("docID", srcID.get()).hasNext)
        srcVertex = g.V().has("docID", srcID.get()).next()
      else {
        println("null inVertex["+srcID.get()+"], there might be something wrong!!!")
        throw new NullPointerException
      }

      if(g.V().has("docID", objID.get().trim).hasNext)
        objVertex = g.V().has("docID", objID.get()).next()
      else {
        println("null outVertex["+objID.get()+"], there might be something wrong!!!")
        throw new NullPointerException
      }

      //edge
      val edgeExists = g.V().has("docID", srcID.get()).outE().as("a").inV().has("docID", objID.get()).select("a").hasNext
      val edge = edgeExists match {
        case true =>
          val eid = g.V().has("docID", srcID.get()).outE().as("a").inV().has("docID", objID.get()).select("a").id().next()
          //println("--used existed--")
          g.E(eid).next()
        case false =>
          srcVertex.addEdge(iter.label, objVertex)
      }

      val piter = iter.propertyPairs.toIterator
      while(piter.hasNext) {
        val pkv = piter.next()
        edge.property(pkv._1, pkv._2.toString)
      }
    }
    graph.tx.commit()
    true
  }

  //delete Document
  def delete(docID: ObjectID) = {
    var re = true
    graph.tx.rollback()
    if(g.V().has("docID", docID.get()).hasNext) {
      //remove in/out edges
      val inIter = g.V().has("docID", docID.get()).inE()
      while(inIter.hasNext) {
        val edge = inIter.next()
        edge.remove()
      }
      val outIter = g.V().has("docID", docID.get()).outE()
      while(outIter.hasNext) {
        val edge = outIter.next()
        edge.remove()
      }
      val v = g.V().has("docID", docID.get()).next()
      v.remove()
    }
    else{
      re = false
    }
    graph.tx().commit()
    re
  }

  //delete DocumentRef
  def delete(srcID:ObjectID, objID:ObjectID) = {
    var re = true
    graph.tx().rollback()
    if(g.V().has("docID", srcID.get()).hasNext && g.V().has("docID", objID.get()).hasNext) {
      var cnt = g.V().has("docID", srcID.get()).outE().as("a").inV().has("docID", objID.get()).select("a").count().next()
      println(cnt)
      while(cnt > 0) {
        println("remove edge here .... " + cnt)
        val eid = g.V().has("docID", srcID.get()).outE().as("a").inV().has("docID", objID.get()).select("a").id().next()
        g.E(eid).next().remove()
        graph.tx().commit()
        cnt = cnt -1
      }
    }
    else {
      re = false
    }
    graph.tx.commit()
    re
  }
}

object TitanSimbaDB{
  def apply(sc:SparkContext, titanConf: String) = new TitanSimbaDB(sc, titanConf)
}