package simba.titan100

import org.apache.spark.SparkContext
import org.slf4j.{Logger, LoggerFactory}
import simba.{SimbaDB, _}

/**
  * Created by liliang on 2017/2/14.
  */

class TitanGraphOfTheGodsExample(titanConf: String,
                                 docObjectFile:String,
                                 linkObjectFile:String) {

  val logger:Logger = LoggerFactory.getLogger("TitanGraphOfTheGodsExample")
  logger.info("Titan Conf " + titanConf + ", Document Object input " + docObjectFile
    + "DocumentRef Object Input " + linkObjectFile)

  //insert Test
  def insertTest(sc: SparkContext): Unit = {
    logger.info("TitanGraphOfTheGodsExample insert begins...")
    System.out.println("TitanGraphOfTheGodsExample insert begins...")
    val docRDD = sc.objectFile[Document](docObjectFile)
    //docRDD.foreach(s => s.simbaPrint())
    System.out.println("TitanGraphOfTheGodsExample linkRDD")
    val refRDD = sc.objectFile[DocumentLink](linkObjectFile)
    //refRDD.foreach(s => s.print())
    var gDB = TitanSimbaDB(sc, titanConf)
    //simbadb
    logger.info("TitanGraphOfTheGodsExample insert benchmark")
    System.out.println("TitanGraphOfTheGodsExample insert benchmark")
    gDB.insert(docRDD)
    gDB.insertLinks(refRDD)
    gDB.docs().foreach(s => s.simbaPrint())

    gDB.close()
    logger.info("TitanGraphOfTheGodsExample  done")
    System.out.println("TitanGraphOfTheGodsExample done")
  }

  //Computation docs/links Test
  def compTest(sc: SparkContext): Unit = {
    logger.info("TitanGraphOfTheGodsExample compTest benchmark begins...")
    System.out.println("TitanGraphOfTheGodsExample compTest benchmark begins...")
    var gDB = TitanSimbaDB(sc, titanConf)

    logger.info("TitanGraphOfTheGodsExample MapReduce Test")
    System.out.println("TitanGraphOfTheGodsExample MapReduce Test")
    val docDist = gDB.docs()
    val linkDist = gDB.links()  //not provide

    docDist.map(doc => doc).foreach(a => a.simbaPrint())
    linkDist.map(r => r).foreach(a => a.print())

    gDB.links().foreach(s => s.print())
    println("TitanGraphOfTheGodsExample count of gDB " + gDB.docs().count())
    println("TitanGraphOfTheGodsExample docs.map.reduce")
    docDist.map(doc=>doc).reduce((doc1, doc2) => doc2).simbaPrint()
    println("TitanGraphOfTheGodsExample links.map.reduce")
    linkDist.map(r => r).reduce((r1, r2) => r2).print()

    logger.info("TitanGraphOfTheGodsExample filter Test")
    System.out.println("TitanGraphOfTheGodsExample filter Test")
    //doc.filter
    docDist.filter(
      d => {
        val name = (d.get("docID") match{
          case Some(a) => a.toString
          case None => ""
        })
        name.compareTo("30605424") == 0
      }).foreach(d=>d.simbaPrint())
    linkDist.filter(r => r.label.compareTo("brother")==0).foreach(r => r.print())

    logger.info("TitanGraphOfTheGodsExample union Test")
    System.out.println("TitanGraphOfTheGodsExample union Test")
    val docRDD = sc.objectFile[Document](docObjectFile)
    val refRDD = sc.objectFile[DocumentLink](linkObjectFile)
    docDist.union(docRDD).foreach(d => d.simbaPrint())
    linkDist.union(refRDD).foreach(r => r.print())

    logger.info("TitanGraphOfTheGodsExample sort Test")
    val dIter = docDist.sortBy[String](d => d.objID().get()).collect().toIterator
    while(dIter.hasNext) println(dIter.next().simbaPrint())

    val rIter = linkDist.sortBy[String](r => r.label).collect().toIterator
    while(rIter.hasNext) println(rIter.next().print())

    logger.info(("TitanGraphOfTheGodsExample groupby Test"))
    System.out.println(("TitanGraphOfTheGodsExample groupby Test"))
    val dgbIter = docDist.sortBy(d =>
      (d.get("http://www.w3.org/1999/02/22-rdf-syntax-ns#type") match {
        case Some(a) =>
          a.toString
        case scala.None =>
          ""})
    ).collect().toIterator
    while(dgbIter.hasNext) println(dgbIter.next().simbaPrint())

    val rgbIter = linkDist.sortBy(r => r.label).collect().toIterator
    while(rgbIter.hasNext) println(rgbIter.next().print())

    gDB.close()
    logger.info("TitanGraphOfTheGodsExample benchmark done")
    System.out.println("TitanGraphOfTheGodsExample benchmark done")
  }

  //run
  def run(sc: SparkContext) = {
    logger.info("TitanGraphOfTheGodsExample Starts...")
    System.out.println("TitanGraphOfTheGodsExample Starts...")

    //titan100ToObjectFile(sc)
    val docRDD = sc.objectFile[Document](docObjectFile)
    docRDD.foreach(s => s.simbaPrint())
    val linkRDD = sc.objectFile[DocumentLink](linkObjectFile)
    println("linkRDD count " + linkRDD.count())
    linkRDD.foreach(s => s.print())
    //    insertTest(sc)
    //    compTest(sc)
    logger.info("TitanGraphOfTheGodsExample Done...")
    System.out.println("TitanGraphOfTheGodsExample Done...")
  }

  //save RDD to HDFS as Object File
  def titan100ToObjectFile(sc: SparkContext): Unit = {
    val titanDB = TitanSimbaDB(sc, titanConf)
    titanDB.docsToObjectFile(docObjectFile)
    titanDB.linksToObjectFile(linkObjectFile)
    titanDB.close()
  }

}

