package simba.titan100.connector

/**
 * Created by cnic-liliang on 2016/12/20.
 */
import org.apache.spark.Partition
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal

import scala.language.existentials

case class TitanPartition[E](index: Int, traversal: GraphTraversal[_, E]) extends Partition
