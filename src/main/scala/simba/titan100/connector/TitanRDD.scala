package simba.titan100.connector

import java.util

import com.thinkaurelius.titan.core.{TitanFactory, TitanGraph}
import org.apache.spark.rdd.RDD
import org.apache.spark.{Partition, SparkContext, TaskContext}
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal
import org.apache.tinkerpop.gremlin.structure.Vertex
import org.apache.tinkerpop.gremlin.structure.Edge

import simba.{Document, DocumentLink, ObjectID}
import simba.titan100.gremlin.connector.Workaround._

import scala.collection.JavaConversions._
import scala.reflect.ClassTag

//The RDD storage of Documents Sets
class TitanDocRDD[E: ClassTag](sc: SparkContext,
                            traversal: GraphTraversal[_, E],
                            numSlices: Int = 0) extends RDD[Document](sc, Nil) {
  private val defaultParallelism = sc.defaultParallelism
  private val readConf = GraphConf(traversal.asAdmin.getGraph.get.asInstanceOf[TitanGraph].configuration)

  override def compute(split: Partition, context: TaskContext): Iterator[Document] = {
    val partition = split.asInstanceOf[TitanPartition[E]]
    val partitionTraversal = partition.traversal.asAdmin
    val graph = TitanFactory.open(readConf.toTitanConf)
    partitionTraversal.setGraph(graph)
    //partitionTraversal
    vertexToIterator(partitionTraversal.toList.toIterator)
  }

  override protected def getPartitions: Array[Partition] = {
    val numElement = cloneTraversal(traversal).count().toList().head
    val numPartitions =
      if (numSlices > 0 && numElement >= numSlices) numSlices
      else if (numElement >= defaultParallelism) defaultParallelism
      else numElement.toInt
    val partitionSize = numElement / numPartitions

    (0 until numPartitions).toArray map { i =>
      val from = partitionSize * i
      val to = partitionSize * (i + 1)
      val partitionTraversal = cloneTraversal(traversal).range(from, to)
      TitanPartition(i, partitionTraversal)
    }
  }

  def vertexToIterator(iter: Iterator[E]):Iterator[Document] = {
    val result:util.ArrayList[Document] = new util.ArrayList[Document]()
    var i = 0
    while(iter.hasNext) {
      val res:Vertex = iter.next().asInstanceOf[Vertex]
      val doc = new Document
      doc.addObjectID(ObjectID(res.property("docID").value().toString))
      doc.setLabel(res.label())
      val keys = res.keys().iterator()
      while (keys.hasNext) {
        val lkey = keys.next()
        if(lkey.compareTo("docID") != 0) {
          val valueIter = res.properties(lkey);
          var value:String = ""
          var cnt = 0
          while(valueIter.hasNext) {
            val subValue = valueIter.next()
            if(cnt > 0) value + ","
            value = value + subValue.value()
          }
          doc.add(lkey.toString, value)
          cnt += 1
        }
      }
      result.add(doc)
      i+=1
    }
    result.toIterator
  }
}

//RDD storages of DocumentRef sets
class TitanRefRDD[E: ClassTag](sc: SparkContext,
                               traversal: GraphTraversal[_, E],
                               numSlices: Int = 0) extends RDD[DocumentLink](sc, Nil) {
  private val defaultParallelism = sc.defaultParallelism
  private val readConf = GraphConf(traversal.asAdmin.getGraph.get.asInstanceOf[TitanGraph].configuration)

  override def compute(split: Partition, context: TaskContext): Iterator[DocumentLink] = {
    val partition = split.asInstanceOf[TitanPartition[E]]
    val partitionTraversal = partition.traversal.asAdmin
    val graph = TitanFactory.open(readConf.toTitanConf)
    partitionTraversal.setGraph(graph)
    //partitionTraversal
    edgeToIterator(partitionTraversal.toList.toIterator)
  }

  override protected def getPartitions: Array[Partition] = {
    val numElement = cloneTraversal(traversal).count().toList().head
    var numPartitions =
      if (numSlices > 0 && numElement >= numSlices) numSlices
      else if (numElement >= defaultParallelism) defaultParallelism
      else numElement.toInt
    if(numPartitions <= 0) numPartitions = 1
    val partitionSize = numElement / numPartitions

    (0 until numPartitions).toArray map { i =>
      val from = partitionSize * i
      val to = partitionSize * (i + 1)
      val partitionTraversal = cloneTraversal(traversal).range(from, to)
      TitanPartition(i, partitionTraversal)
    }
  }

  def edgeToIterator(iter: Iterator[E]):Iterator[DocumentLink] = {
    val result:util.ArrayList[DocumentLink] = new util.ArrayList[DocumentLink]()
    var i = 0
    while(iter.hasNext) {
      val ev:Edge = iter.next().asInstanceOf[Edge]
      val ref = new DocumentLink
      ref.link(ev.label())
      ref.refSet(ev.inVertex().property("docID").value().toString, ev.outVertex().property("docID").value().toString)
      ref.link(ev.label())
      //properties
      val keys = ev.keys().iterator()
      while (keys.hasNext) {
        val lkey = keys.next()
        ref.add(lkey.toString, (ev.property(lkey).value().toString))
      }
      result.add(ref)
      i+=1
    }
    result.toIterator
  }
}
