package simba.titan100

import org.apache.spark.SparkContext
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal

import scala.reflect.ClassTag

package object connector {
  implicit class SparkContextFunctions[E: ClassTag](traversal: GraphTraversal[_, E]) {
    def toDocRDD(numSlice: Int = 1)(implicit sc: SparkContext) = new TitanDocRDD(sc, traversal, numSlice)
    def toRefRDD(numSlice: Int = 1)(implicit sc: SparkContext) = new TitanRefRDD(sc, traversal, numSlice)
    }
}