package simba

import org.apache.spark.rdd.RDD

/**
  * Created by liliang on 2016/12/7.
  */

//SimbaDB
class SimbaDB{
  //the operations of SimbaDB
  //def query(simbaQL: String): DistRows = {new RDD("")}

  def insert(docRDD:RDD[Document]) = {true}
  def insertLinks(refRDD:RDD[DocumentLink]) = {true}

  def docs(): RDD[Document] = null
  def links(): RDD[DocumentLink] = null
}
